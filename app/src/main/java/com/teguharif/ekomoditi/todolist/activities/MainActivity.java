package com.teguharif.ekomoditi.todolist.activities;

import android.content.DialogInterface;
import android.content.Intent;
//import android.icu.text.SimpleDateFormat;
import java.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.teguharif.ekomoditi.todolist.R;
import com.teguharif.ekomoditi.todolist.db.DataBase;
import com.teguharif.ekomoditi.todolist.model.ToDo;

public class MainActivity extends AppCompatActivity {

    private FloatingActionButton fabNewTodo;
    private LinearLayout scrollContent;
    private DataBase db;
    private ToDo[] currentToDos;

    private View.OnClickListener fabNewTodoClick  = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, EditTodoActivity.class);
            intent.putExtra("todoID", 0);
            startActivity(intent);
        }
    };

    private void refreshContentLayout(){
        scrollContent = (LinearLayout) findViewById(R.id.scrollContent);
        scrollContent.removeAllViews();

        db = DataBase.getInstance();
        currentToDos = db.selectAll();

        if(!(currentToDos==null)){
            for(int i = 0; i<currentToDos.length; i++){
                View myItem = getLayoutInflater().inflate(R.layout.todo_item, null);
                final int itemID = currentToDos[i].getId();


                TextView txtTitle = (TextView) myItem.findViewById(R.id.txtTitle);
                TextView txtTaskDate = (TextView) myItem.findViewById(R.id.txtTaskDate);

                SimpleDateFormat sdf = new SimpleDateFormat("M/dd/yyyy");
                String date = sdf.format(currentToDos[i].getTaskDate());

                txtTitle.setText( currentToDos[i].getTitle());
                txtTaskDate.setText(date);


                ImageButton editBtn = (ImageButton) myItem.findViewById(R.id.editButton);
                editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, EditTodoActivity.class);
                        intent.putExtra("todoID", itemID);
                        startActivity(intent);
                    }
                });

                ImageButton deleteBtn = (ImageButton) myItem.findViewById(R.id.deleteButton);
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                        alert.setTitle("Warning");
                        alert.setMessage("Are you sure to delete this item?");
                        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                db.delete(itemID);

                                Toast toast = Toast.makeText( getApplicationContext(), "ToDo deleted.", Toast.LENGTH_LONG );
                                toast.show();
                                refreshContentLayout();
                            }
                        });
                        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        alert.show();
                    }
                });


                scrollContent.addView(myItem);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fabNewTodo = (FloatingActionButton) findViewById(R.id.fab_new_todo);
        fabNewTodo.setOnClickListener(fabNewTodoClick);
        db.getInstance();
    }

    @Override
    public void onResume(){
        super.onResume();
        refreshContentLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
}
