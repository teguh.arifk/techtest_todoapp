package com.teguharif.ekomoditi.todolist.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.teguharif.ekomoditi.todolist.R;
import com.teguharif.ekomoditi.todolist.db.DataBase;
import com.teguharif.ekomoditi.todolist.model.ToDo;

import java.util.Date;

public class EditTodoActivity extends AppCompatActivity {

    private Button saveButton;
    private Button cancelButton;
    private EditText editTodoTitle;
    private CheckBox checkBoxCompleted;
    private ToDo currentTodo;
    private String todoTitle;
    private EditText editTaskDate;

    private View.OnClickListener saveButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            todoTitle = editTodoTitle.getText().toString();
            if(!todoTitle.isEmpty() ){
                if(currentTodo==null){
                    currentTodo = new ToDo();
                }

                currentTodo.setTitle(todoTitle);
                currentTodo.setCompleted(checkBoxCompleted.isChecked());
                currentTodo.setTaskDate(new Date());

                if(currentTodo.getId()==0){
                    DataBase.getInstance().insert(currentTodo);
                }else{
                    DataBase.getInstance().update(currentTodo);
                    System.out.println("TODO UPDATED");
                }


                Toast toast = Toast.makeText( getApplicationContext(), "ToDo Saved.", Toast.LENGTH_SHORT );
                toast.show();

                finish();


            }else{
                final AlertDialog.Builder alert = new AlertDialog.Builder(EditTodoActivity.this);
                alert.setTitle("Warning");
                alert.setMessage("Please, provide a title.");
                alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();
            }

        }
    };

    private View.OnClickListener cancelButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast toast = Toast.makeText( getApplicationContext(), "Changes discarded.", Toast.LENGTH_SHORT );
            toast.show();
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_todo);

        saveButton = (Button) findViewById(R.id.btnSave);
        cancelButton = (Button) findViewById(R.id.btnCancel);
        editTodoTitle = (EditText) findViewById(R.id.editTodoTitle);
        checkBoxCompleted = (CheckBox) findViewById(R.id.checkCompleted);

        saveButton.setOnClickListener(saveButtonClick);
        cancelButton.setOnClickListener(cancelButtonClick);

        int todoID = getIntent().getExtras().getInt("todoID");
        if(todoID!=0){
            currentTodo = DataBase.getInstance().select(todoID);
            editTodoTitle.setText(currentTodo.getTitle());
            checkBoxCompleted.setChecked(currentTodo.isCompleted());
        }else{
            showSoftKeyboard();
        }
    }

    public void showSoftKeyboard(){
        InputMethodManager inputMethodManager =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void hideSoftKeyboard(){
        InputMethodManager inputMethodManager =
                (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideSoftKeyboard();
    }
}
