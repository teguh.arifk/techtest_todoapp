package com.teguharif.ekomoditi.todolist.model;

import java.util.Date;

public class ToDo {
    private int id;
    private String title;
    private Date taskDate;
    private boolean completed;

    public ToDo(int id, String title, Date taskDate) {
        this.id = id;
        this.title = title;
        this.taskDate = taskDate;
    }

    public ToDo(){
        this.id=0;
    }

    public boolean isCompleted(){
        return completed;
    }

    public void setCompleted(boolean b){
        this.completed = b;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

}
